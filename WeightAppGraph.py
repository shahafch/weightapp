import matplotlib.pyplot as plt


def build_graph(date_axe, weight_axe):

    # Plotting the points
    plt.plot(date_axe, weight_axe)

    # naming the x axis
    plt.xlabel('Date')

    # naming the y axis
    plt.ylabel('Weight')

    # giving a title to my graph
    plt.title('Your Weight By The Date')

    # function to show the plot
    plt.show()
