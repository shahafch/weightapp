import mysql.connector


def connect_db():
    weight_db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="SH4532425",
        database="weightdb",
        auth_plugin='mysql_native_password')
    db_cursor = weight_db.cursor()
    return weight_db, db_cursor


def insert_info(weight_db, db_cursor, weight, date):
    new_record_sql = "INSERT INTO weight(weight, date) VALUES (%s, %s)"
    new_record = (weight, date)
    db_cursor.execute(new_record_sql, new_record)
    weight_db.commit()


def delete_last_record(weight_db, db_cursor):
    db_cursor.execute("SELECT MAX(weight_id) AS maximum FROM weight")
    max_weight_id = db_cursor.fetchall()
    str_max = str(max_weight_id[0][0])

    delete_record_sql = "DELETE FROM weight ORDER BY weight_id DESC LIMIT 1"
    db_cursor.execute(delete_record_sql)
    weight_db.commit()

    db_cursor.execute("ALTER TABLE weight AUTO_INCREMENT =" + str_max)


def get_coord_data(db_cursor):
    date_coord = []
    weight_coord = []
    db_cursor.execute("SELECT * FROM weight")
    records = db_cursor.fetchall()
    if not records == []:
        for row in records:
            readable_row = str(row).replace(')', '').replace('(', '').replace('u\'', '').replace("'", "")
            split_info = readable_row.split(',')
            weight_coord.append(int(split_info[0]))
            date_coord.append(split_info[1])
        return date_coord, weight_coord
    else:
        return False, False

